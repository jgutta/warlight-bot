#---------------------------------------------------------------------#
# Warlight AI Challenge - Starter Bot                                 #
# ============                                                        #
#                                                                     #
# Last update: 20 Mar, 2014                                           #
#                                                                     #
# @author Jackie <jackie@starapple.nl>                                #
# @version 1.0                                                        #
# @license MIT License (http://opensource.org/licenses/MIT)           #
#---------------------------------------------------------------------#

from math import fmod, pi
from sys import stderr, stdin, stdout
from time import clock
class Bot(object):
    '''
    Main bot class
    '''
    def __init__(self):
        '''
        Initializes a map instance and an empty dict for settings
        '''
        self.NA = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
        self.SA = ['10', '11', '12', '13']
        self.EU = ['14', '15', '16', '17', '18', '19', '20']
        self.Af = ['21', '22', '23', '24', '25', '26']
        self.As = ['27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38']
        self.Aus = ['39', '40', '41', '42']
        self.result = []
        self.Genetics = Genetics()
        self.settings = {}
        self.map = Map()

    def run(self):
        '''
        Main loop

        Keeps running while being fed data from stdin.
        Writes output to stdout, remember to flush!
        '''
        while not stdin.closed:
            try:
                rawline = stdin.readline()

                # End of file check
                if len(rawline) == 0:
                    break

                line = rawline.strip()

                # Empty lines can be ignored
                if len(line) == 0:
                    continue

                parts = line.split()

                command = parts[0]

                # All different commands besides the opponents' moves
                if command == 'settings':
                    self.update_settings(parts[1:])

                elif command == 'opponent_moves':
                    self.passTurn();

                elif command == 'setup_map':
                    self.setup_map(parts[1:])

                elif command == 'update_map':
                    self.update_map(parts[1:])

                elif command == 'pick_starting_regions':
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()

                elif command == 'go':

                    sub_command = parts[1]

                    if sub_command == 'place_armies':

                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()

                    elif sub_command == 'attack/transfer':

                        stdout.write(self.attack_transfer() + '\n')
                        stdout.flush()

                    else:
                        stderr.write('Unknown sub command: %s\n' % (sub_command))
                        stderr.flush()

                else:
                    stderr.write('Unknown command: %s\n' % (command))
                    stderr.flush()
            except EOFError:
                return

    def update_settings(self, options):
        '''
        Method to update game settings at the start of a new game.
        '''
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        '''
        Method to set up essential map data given by the server.
        '''
        map_type = options[0]

        for i in range(1, len(options), 2):

            if map_type == 'super_regions':

                super_region = SuperRegion(options[i], int(options[i + 1]))
                self.map.super_regions.append(super_region)

            elif map_type == 'regions':

                super_region = self.map.get_super_region_by_id(options[i + 1])
                region = Region(options[i], super_region)

                self.map.regions.append(region)
                super_region.regions.append(region)

            elif map_type == 'neighbors':

                region = self.map.get_region_by_id(options[i])
                neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]

                for neighbour in neighbours:
                    region.neighbours.append(neighbour)
                    neighbour.neighbours.append(region)

        if map_type == 'neighbors':

            for region in self.map.regions:

                if region.is_on_super_region_border:
                    continue

                for neighbour in region.neighbours:

                    if neighbour.super_region.id != region.super_region.id:

                        region.is_on_super_region_border = True
                        neighbour.is_on_super_region_border = True

    def update_map(self, options):
        '''
        Method to update our map every round.
        '''
        for i in range(0, len(options), 3):
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])

    def passTurn(self):
        ''' dummy method to pass turn'''
        vturn = 1

    def pick_starting_regions(self, options):
        '''
        Method to select our initial starting regions.

        1 - 9 NA, 10-13 SA, 14-20 EU, 21-26 Af, 27-38 As, 39-42 Aus
        '''
        start = []
        NA = list(set(self.NA) & set(options))
        SA = list(set(self.SA)& set(options))
        EU = list(set(self.EU)& set(options))
        Af = list(set(self.Af)& set(options))
        As = list(set(self.As)& set(options))
        Aus = list(set(self.Aus)& set(options))
        if len(SA) != 0:
            start.extend(SA)
        if len(Aus) != 0:
            start.extend(Aus)
        if len(NA) != 0:
            start.extend(NA)
        if len(Af) != 0:
            start.extend(Af)
        if len(EU) != 0:
            start.extend(EU)
        if len(As) != 0:
            start.extend(As)
        
        return start[0] + ' ' + start[1] + ' ' + start[2] + ' ' + start[3] + ' ' + start[4] + ' ' + start[5]

    def intersect(self, a, b):
        '''For some reason, this method doesnt work when run on the online server'''
        return list(set(a) & set(b))
        
    def getKey(self, item):
        return item[1]

    def regionify(self, regList):
        try:
            ret = []
            NA = list(set(self.NA) & set(regList))
            NAown = (len(NA)/ 9)%1
            ret.append([NA, NAown])
            SA = list(set(self.SA) & set(regList))
            SAown = (len(SA)/ 4)%1
            ret.append([SA, SAown])
            EU = list(set(self.EU) & set(regList))
            EUown = (len(EU)/ 6)%1
            ret.append([EU, EUown])
            Af = list(set(self.Af) & set(regList))
            Afown = (len(Af)/ 6)%1
            ret.append([Af, Afown])
            As = list(set(self.As) & set(regList))
            Asown = (len(As)/ 11)%1
            ret.append([As, Asown])
            Aus = list(set(self.Aus) & set(regList))
            Ausown = (len(Aus)/ 4)%1
            ret.append([Aus, Ausown])
            ret = sorted(ret, reverse = True, key = self.getKey)
            return ret
        except TypeError as err:
            print(regList, self.settings['your_bot'], err)
    
    def place_troops(self):
        '''
        Method to place our troops.

        Prioritized regions by percent owned, loops through and adds troops by percent owned in region
        '''
        placements = []
        troops_remaining = int(self.settings['starting_armies'])
        regioned = self.regionify(owned_regions)

        """neutral_regions, enemy_regions = self.map.get_neighbor_status(self.settings['your_bot'])
        if len(enemy_regions) > len(neutral_regions) / 6:
            self.strat = 1
        elif len(enemy_regions) > len(neutral_regions) / 3:
            self.strat = 2
        else:
            self.strat = 0"""
        region_index = 0
        troops_remaining = int(self.settings['starting_armies'])
        
        owned_regions = self.map.borderLands(self.settings['your_bot'])
        duplicated_regions = owned_regions * (3 + int(troops_remaining / 2))
        shuffled_regions = Random.shuffle(duplicated_regions)
        
        while troops_remaining:

            region = shuffled_regions[region_index]
            
            if troops_remaining > 1:

                placements.append([region.id, 2])

                region.troop_count += 2
                troops_remaining -= 2
                
            else:

                 placements.append([region.id, 1])

                 region.troop_count += 1
                 troops_remaining -= 1

            region_index += 1
        
        return ', '.join(['%s place_armies %s %d' % (self.settings['your_bot'], placement[0],
            placement[1]) for placement in placements])

    def attack_transfer(self):
        '''
        Method to attack another region or transfer troops to allied regions.

        Currently checks whether a region has more than six troops placed to attack,
        or transfers if more than 1 unit is available.
        '''
        attack_transfers = []

        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        temp = '.'
        for region in owned_regions:
            temp = self.Genetics.getMove(region)
            if temp != '.':
                attack_transfers.append(temp)
                temp = '.'

        """for region in owned_regions:
            neighbours = list(region.neighbours)
            while len(neighbours) > 0:
                target_region = neighbours[Random.randrange(0, len(neighbours))]
                if region.owner != target_region.owner and region.troop_count > 6:
                    attack_transfers.append([region.id, target_region.id, 5])
                    region.troop_count -= 5
                elif region.owner == target_region.owner and region.troop_count > 1:
                    attack_transfers.append([region.id, target_region.id, region.troop_count - 1])
                    region.troop_count = 1
                else:
                    neighbours.remove(target_region)

        if len(attack_transfers) == 0:
            return 'No moves'"""
       
        return ', '.join(attack_transfers)

        
class Genetics(object):
    # move is an array consisting of [startRegion, destination, proposed move]
    def fitness(self,move):
        fit = 0
        bonus = 1
        if self.isBorder(move[0]):
            if move[0].owner == move[1].owner:
                if self.isBorder(move[1]):
                    fit = int(move[2])/(move[1].troop_count*move[0].troop_count)
                else:
                    fit = 0
            else:
                fit = (-int(move[1].troop_count)/(move[0].troop_count) + .6*int(move[2]))* move[2]
                       
        else:
            if self.isBorder(move[1]):
                fit = int(move[2])
            else:
                fit = -15 * move[2]

        if move[1].owner == 'neutral':
            bonus = 2
        elif  move[1].owner == move[0].owner:
            bonus = .5
        else:
            bonus = 4
        return (fit * bonus) + move[2]
                
            
    def mate(self,moveA, moveB):
        moveChild = [moveA[0]]
        if moveA[1].id == moveB[1].id:
            moveChild.extend([moveA[1]])
            moveChild.extend([max(moveA[2], moveB[2])])
        elif moveA[1].troop_count < moveB[1].troop_count:
            moveChild.extend([moveA[1]])
            moveChild.extend([max(moveA[2], moveB[2])])
        else:
            moveChild.extend([moveB[1]])
            moveChild.extend([max(moveA[2], moveB[2])])
        
        return [moveChild, moveA, moveB]
       
        
        
    def mutate(self,region):
        troopMove = Random.randrange(1, int(region.troop_count))
        moveTo = Random.shuffle(list(region.neighbours))[0]
        newMove = [region, moveTo, troopMove]
        return newMove
        
        
    def isBorder(self, reg):
        ret = False
        for region in reg.neighbours:
            if region.owner != reg.owner:
                ret = True
        return ret
    
    def getMove(self, region):
        if region.troop_count == 1:
            return '.'
        else:
            bestMove = self.mutate(region)
            for i in range(0, 3):
                children = [self.mutate(region)  for _ in range(500)] + [bestMove]
                best1 = max(children[:250], key=self.fitness)
                best2 = max(children[250:], key=self.fitness)
                bestMove = max(self.mate(best1, best2), key=self.fitness)
        if(bestMove[0].owner == bestMove[1].owner):
            bestMove[0].troop_count -= bestMove[2]
            bestMove[1].troop_count += bestMove[2]
        else:
            bestMove[0].troop_count -= bestMove[2]
            
                
        strMove = region.owner + ' attack/transfer ' + bestMove[0].id + ' ' + bestMove[1].id+ ' ' + str(bestMove[2])
        return strMove

class Map(object):
    '''
    Map class
    '''
    def __init__(self):
        '''
        Initializes empty lists for regions and super regions.
        '''
        self.activeReg = []
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        '''
        Returns a region instance by id.
        '''
        return [region for region in self.regions if region.id == region_id][0]

    def get_super_region_by_id(self, super_region_id):
        '''
        Returns a super region instance by id.
        '''
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        '''
        Returns a list of region instances owned by `owner`.
        
        
        
        ret = []
        for i in owned:
            ret.insert(0,self.get_region_by_id(i))
        return ret
        '''
        return [region for region in self.regions if region.owner == owner]

    def get_neutral_neighbors(self, owner):
        owned = self.get_owned_regions(self, owner)
        neutral = []
        for region in owned:
            for neighbor in list(region.neighbours):
                if neighbor.owner == 'neutral':
                    neutral.extend(neighbor)
        return neutral

    def get_enemy_neighbors(self, owner):
        owned = self.get_owned_regions(self, owner)
        enemy = []
        for region in owned:
            for neighbor in list(region.neighbours):
                if neighbor.owner != 'neutral' and neighbor.owner != owner:
                    enemy.extend(neighbor)
        return enemy
    def isBorder(self, reg):
        ret = False
        for region in reg.neighbours:
            if region.owner != reg.owner:
                ret = True
        return ret

    def borderLands(self, owner):
        return[region for region in self.regions if self.isBorder(region) and region.owner == owner]
    
    def get_neighbor_status(self, owner):
        owned = self.get_owned_regions(self, owner)
        neutral = []
        enemy = []
        for region in owned:
            for neighbor in list(region.neighbours):
                if neighbor.owner == 'neutral':
                    neutral.extend(neighbor)
                elif neighbor.owner != owner:
                    enemy.extend(neighbor)
        return neutral, enemy

class SuperRegion(object):
    '''
    Super Region class
    '''
    def __init__(self, super_region_id, worth):
        '''
        Initializes with an id, the super region's worth and an empty lists for
        regions located inside this super region
        '''
        self.id = super_region_id
        self.worth = worth
        self.regions = []


class Region(object):
    '''
    Region class
    '''
    def __init__(self, region_id, super_region):
        '''
        '''
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False


class Random(object):
    '''
    Random class
    '''
    @staticmethod
    def randrange(min, max):
        '''
        A pseudo random number generator to replace random.randrange

        Works with an inclusive left bound and exclusive right bound.
        E.g. Random.randrange(0, 5) in [0, 1, 2, 3, 4] is always true
        '''
        return min + int(fmod(pow(clock() + pi, 2), 1.0) * (max - min))

    @staticmethod
    def shuffle(items):
        '''
        Method to shuffle a list of items
        '''
        i = len(items)
        while i > 1:
            i -= 1
            j = Random.randrange(0, i)
            items[j], items[i] = items[i], items[j]
        return items
        
"""class warTask(Task):
    
    def __init__(self, environment):
        self.env = environment
    
    def getObservation(self):
        return self.env.getSensors()
        
    def performAction(self, action):
        self.env.performAction(action)
    
    def getReward(self):
        
class warEnv(Environment):
    
    def __init__(self):
        indim = 42
        outdim = 2
    
    def getSensors(self):
        return self.map.get_owned_regions(self.settings['your_bot'])"""
        
        
    
if __name__ == '__main__':
    '''
    Not used as module, so run
    '''
    Bot().run()
