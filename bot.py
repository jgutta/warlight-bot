#---------------------------------------------------------------------#
# Warlight AI Challenge - Starter Bot                                 #
# ============                                                        #
#                                                                     #
# Last update: 20 Mar, 2014                                           #
#                                                                     #
# @author Jackie <jackie@starapple.nl>                                #
# @version 1.0                                                        #
# @license MIT License (http://opensource.org/licenses/MIT)           #
#---------------------------------------------------------------------#

from math import fmod, pi
from sys import stderr, stdin, stdout
from time import clock
class Bot(object):
    '''
    Main bot class
    '''
    def __init__(self):
        '''
        Initializes a map instance and an empty dict for settings
        '''
        self.result = []
        self.settings = {}
        self.map = Map()

    def run(self):
        '''
        Main loop

        Keeps running while being fed data from stdin.
        Writes output to stdout, remember to flush!
        '''
        while not stdin.closed:
            try:
                rawline = stdin.readline()

                # End of file check
                if len(rawline) == 0:
                    break

                line = rawline.strip()

                # Empty lines can be ignored
                if len(line) == 0:
                    continue

                parts = line.split()

                command = parts[0]

                # All different commands besides the opponents' moves
                if command == 'settings':
                    self.update_settings(parts[1:])

                elif command == 'opponent_moves':

                elif command == 'setup_map':
                    self.setup_map(parts[1:])

                elif command == 'update_map':
                    self.update_map(parts[1:])

                elif command == 'pick_starting_regions':
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()

                elif command == 'go':

                    sub_command = parts[1]

                    if sub_command == 'place_armies':

                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()

                    elif sub_command == 'attack/transfer':

                        stdout.write(self.attack_transfer() + '\n')
                        stdout.flush()

                    else:
                        stderr.write('Unknown sub command: %s\n' % (sub_command))
                        stderr.flush()

                else:
                    stderr.write('Unknown command: %s\n' % (command))
                    stderr.flush()
            except EOFError:
                return

    def update_settings(self, options):
        '''
        Method to update game settings at the start of a new game.
        '''
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        '''
        Method to set up essential map data given by the server.
        '''
        map_type = options[0]

        for i in range(1, len(options), 2):

            if map_type == 'super_regions':

                super_region = SuperRegion(options[i], int(options[i + 1]))
                self.map.super_regions.append(super_region)

            elif map_type == 'regions':

                super_region = self.map.get_super_region_by_id(options[i + 1])
                region = Region(options[i], super_region)

                self.map.regions.append(region)
                super_region.regions.append(region)

            elif map_type == 'neighbors':

                region = self.map.get_region_by_id(options[i])
                neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]

                for neighbour in neighbours:
                    region.neighbours.append(neighbour)
                    neighbour.neighbours.append(region)

        if map_type == 'neighbors':

            for region in self.map.regions:

                if region.is_on_super_region_border:
                    continue

                for neighbour in region.neighbours:

                    if neighbour.super_region.id != region.super_region.id:

                        region.is_on_super_region_border = True
                        neighbour.is_on_super_region_border = True

    def update_map(self, options):
        '''
        Method to update our map every round.
        '''
        for i in range(0, len(options), 3):
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])

    def pick_starting_regions(self, options):
        '''
        Method to select our initial starting regions.

        1 - 9 NA, 10-13 SA, 14-20 EU, 21-26 Af, 27-38 As, 39-42 Aus
        '''
        start = []
        NA = self.intersect(list(range(1, 10)), options)
        SA = self.intersect(list(range(10, 14)), options)
        EU = self.intersect(list(range(14, 21)), options)
        Af = self.intersect(list(range(21, 27)), options)
        As = self.intersect(list(range(27, 39)), options)
        Aus = self.intersect(list(range(39, 43)), options)
        if len(SA) != 0:
            start.extend(SA)
        if len(Aus) != 0:
            start.extend(Aus)
        if len(NA) != 0:
            start.extend(NA)
        if len(Af) != 0:
            start.extend(Af)
        if len(EU) != 0:
            start.extend(EU)
        if len(As) != 0:
            start.extend(As)

        return start[:6]

    def intersect(a, b):
        return list(set(a) & set(b))
        
    def getKey(item):
        return item[1]

    def regionify(self, regList):
        ret = []
        NA = self.intersect(list(range(1, 10)), regList)
        NAown = (len(NA)/ 9)%1
        ret.append([NA, NAown])
        SA = self.intersect(list(range(10, 14)), regList)
        SAown = (len(SA)/ 4)%1
        ret.append([SA, SAown])
        EU = self.intersect(list(range(14, 21)), regList)
        EUown = (len(EU)/ 6)%1
        ret.append([EU, EUown])
        Af = self.intersect(list(range(21, 27)), regList)
        Afown = (len(Af)/ 6)%1
        ret.append([Af, Afown])
        As = self.intersect(list(range(27, 39)), regList)
        Asown = (len(As)/ 11)%1
        ret.append([As, Asown])
        Aus = self.intersect(list(range(39, 43)), regList)
        Ausown = (len(Aus)/ 4)%1
        ret.append([Aus, Ausown])
        ret = sorted(ret, reverse = True, key = self.getKey)
        return ret
    
    def place_troops(self):
        '''
        Method to place our troops.

        Prioritized regions by percent owned, loops through and adds troops by percent owned in region
        '''
        placements = []
        troops_remaining = int(self.settings['starting_armies'])
        loop = 0
        owned_regions = self.map.get_owned_regions(self.settings['your_bot']).sort()
        regioned = self.regionify(owned_regions)

        """neutral_regions, enemy_regions = self.map.get_neighbor_status(self.settings['your_bot'])
        if len(enemy_regions) > len(neutral_regions) / 6:
            self.strat = 1
        elif len(enemy_regions) > len(neutral_regions) / 3:
            self.strat = 2
        else:
            self.strat = 0"""

        while troops_remaining:

            if len(regioned[loop][0]) > 0:
                region = regioned[loop][0][0]
            else:
                while len(regioned[loop][0]) == 0:
                    loop += 1
                    loop = loop % 6
                region = regioned[loop][0][0]

            if troops_remaining > 2:

                placements.append([region.id, 3])

                region.troop_count += 3
                troops_remaining -= 3

            else:

                 placements.append([region.id, troops_remaining])

                 region.troop_count += troops_remaining
                 troops_remaining = 0

            loop += 1

        return ', '.join(['%s place_armies %s %d' % (self.settings['your_bot'], placement[0],
            placement[1]) for placement in placements])

    def attack_transfer(self):
        '''
        Method to attack another region or transfer troops to allied regions.

        Currently checks whether a region has more than six troops placed to attack,
        or transfers if more than 1 unit is available.
        '''
        attack_transfers = []

        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        temp = '.'
        for region in owned_regions:
            temp = self.Genetics.getMove(region)
            if temp != '.':
                attack_transfers.extend(temp)
                temp = '.'

        """for region in owned_regions:
            neighbours = list(region.neighbours)
            while len(neighbours) > 0:
                target_region = neighbours[Random.randrange(0, len(neighbours))]
                if region.owner != target_region.owner and region.troop_count > 6:
                    attack_transfers.append([region.id, target_region.id, 5])
                    region.troop_count -= 5
                elif region.owner == target_region.owner and region.troop_count > 1:
                    attack_transfers.append([region.id, target_region.id, region.troop_count - 1])
                    region.troop_count = 1
                else:
                    neighbours.remove(target_region)

        if len(attack_transfers) == 0:
            return 'No moves'"""

        return attack_transfers.join(', ')

class Genetics(object):
    def fitness(move):
        fit = 0
        
        if move[3] == move[5]:
            fit = (move[4] + move[2])/10
        else:
            fit = -move[4] + .6*move[2]
        return fit
            
    def mate(moveA, moveB):
        moveChild = [moveA[0]]
        if moveA[1] == moveB[1]:
            moveChild.extend(moveA[1])
            moveChild.extend(max(moveA[2], moveB[2]))
            moveChild.extend(moveA[3:])
        elif moveA[4] < moveB[4]:
            moveChild.extend(moveA[1])
            moveChild.extend(max(moveA[2], moveB[2]))
            moveChild.extend(moveA[3:])
        else:
            moveChild.extend(moveB[1])
            moveChild.extend(max(moveA[2], moveB[2]))
            moveChild.extend(moveB[3:])
        
        return [moveChild, moveA, moveB]
       
        
        
    def mutate(region):
        troopMove = Random.randrange(0, region.troop_count)
        moveTo = Random.shuffle(list(region.neighbors))
        newMove = [region.id, moveTo[0].id, troopMove, moveTo[0].owner, moveTo[0].troop_count, region.owner]
        return newMove
        
        

    
    def getMove(self, region):
        if region.troop_count == 1:
            return '.'
        else:
            bestMove = self.mutate(region)
            for i in range(0, 3):
                children = [self.mutate(region)  for _ in range(100)] + [bestMove]
                best1 = max(children[:50], key=self.fitness)
                best2 = max(children[50:], key=self.fitness)
                bestMove = max(self.mate(best1, best2), key=self.fitness)
                
        strMove = region.owner + ' attack/transfer ' + bestMove[0] + ' ' + bestMove[1]+ ' ' + bestMove[2]
        return strMove

class Map(object):
    '''
    Map class
    '''
    def __init__(self):
        '''
        Initializes empty lists for regions and super regions.
        '''
        self.activeReg = []
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        '''
        Returns a region instance by id.
        '''
        return [region for region in self.regions if region.id == region_id][0]

    def get_super_region_by_id(self, super_region_id):
        '''
        Returns a super region instance by id.
        '''
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        '''
        Returns a list of region instances owned by `owner`.
        '''
        return [region for region in self.regions if region.owner == owner]

    def get_neutral_neighbors(self, owner):
        owned = self.get_owned_regions(self, owner)
        neutral = []
        for region in owned:
            for neighbor in list(region.neighbours):
                if neighbor.owner == 'neutral':
                    neutral.extend(neighbor)
        return neutral

    def get_enemy_neighbors(self, owner):
        owned = self.get_owned_regions(self, owner)
        enemy = []
        for region in owned:
            for neighbor in list(region.neighbours):
                if neighbor.owner != 'neutral' and neighbor.owner != owner:
                    enemy.extend(neighbor)
        return enemy

    def get_neighbor_status(self, owner):
        owned = self.get_owned_regions(self, owner)
        neutral = []
        enemy = []
        for region in owned:
            for neighbor in list(region.neighbours):
                if neighbor.owner == 'neutral':
                    neutral.extend(neighbor)
                elif neighbor.owner != owner:
                    enemy.extend(neighbor)
        return neutral, enemy

class SuperRegion(object):
    '''
    Super Region class
    '''
    def __init__(self, super_region_id, worth):
        '''
        Initializes with an id, the super region's worth and an empty lists for
        regions located inside this super region
        '''
        self.id = super_region_id
        self.worth = worth
        self.regions = []


class Region(object):
    '''
    Region class
    '''
    def __init__(self, region_id, super_region):
        '''
        '''
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False


class Random(object):
    '''
    Random class
    '''
    @staticmethod
    def randrange(min, max):
        '''
        A pseudo random number generator to replace random.randrange

        Works with an inclusive left bound and exclusive right bound.
        E.g. Random.randrange(0, 5) in [0, 1, 2, 3, 4] is always true
        '''
        return min + int(fmod(pow(clock() + pi, 2), 1.0) * (max - min))

    @staticmethod
    def shuffle(items):
        '''
        Method to shuffle a list of items
        '''
        i = len(items)
        while i > 1:
            i -= 1
            j = Random.randrange(0, i)
            items[j], items[i] = items[i], items[j]
        return items
        
"""class warTask(Task):
    
    def __init__(self, environment):
        self.env = environment
    
    def getObservation(self):
        return self.env.getSensors()
        
    def performAction(self, action):
        self.env.performAction(action)
    
    def getReward(self):
        
class warEnv(Environment):
    
    def __init__(self):
        indim = 42
        outdim = 2
    
    def getSensors(self):
        return self.map.get_owned_regions(self.settings['your_bot'])"""
        
        
    
if __name__ == '__main__':
    '''
    Not used as module, so run
    '''
    Bot().run()
